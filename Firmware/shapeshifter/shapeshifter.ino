/*
 * SHAPESHIFTER DIY OPEN SOURCE DRUM MACHINE
 * by FASELUNARE.COM
 * based on the DIY DRUM MACHINE by Sebastian Tomczak
 *  
 * https://github.com/faselunare/shapeshifter 
 * Released under the GPLv3 license. 
 * 
 * shapeshifter.ino
 * 01.12.2018
 * v.1.1.1
 */

#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

// GUItool: begin automatically generated code
AudioPlaySdWav           track1;         //xy=140,163
AudioPlaySdWav           track2;         //xy=140,205
AudioPlaySdWav           track3;         //xy=140,263
AudioPlaySdWav           track4;         //xy=140,313
AudioMixer4              trk1_2mix;      //xy=322,186
AudioMixer4              trk3_4mix;      //xy=325,286
AudioSynthWaveformDc     dc1;            //xy=520,309
AudioMixer4              source_mixer;   //xy=546,240
AudioEffectMultiply      multiply1;      //xy=706,291
AudioFilterBiquad        filter1;        //xy=870,299
AudioEffectBitcrusher    bitcrusher1;    //xy=1060,297
AudioOutputAnalog        dac1;           //xy=1234,297
AudioConnection          patchCord1(track1, 0, trk1_2mix, 0);
AudioConnection          patchCord2(track1, 1, trk1_2mix, 1);
AudioConnection          patchCord3(track2, 0, trk1_2mix, 2);
AudioConnection          patchCord4(track2, 1, trk1_2mix, 3);
AudioConnection          patchCord5(track3, 0, trk3_4mix, 0);
AudioConnection          patchCord6(track3, 1, trk3_4mix, 1);
AudioConnection          patchCord7(track4, 0, trk3_4mix, 2);
AudioConnection          patchCord8(track4, 1, trk3_4mix, 3);
AudioConnection          patchCord9(trk1_2mix, 0, source_mixer, 0);
AudioConnection          patchCord10(trk3_4mix, 0, source_mixer, 1);
AudioConnection          patchCord11(dc1, 0, multiply1, 1);
AudioConnection          patchCord12(source_mixer, 0, multiply1, 0);
AudioConnection          patchCord13(multiply1, filter1);
AudioConnection          patchCord14(filter1, bitcrusher1);
AudioConnection          patchCord15(bitcrusher1, dac1);
// GUItool: end automatically generated code


// Use these with the Teensy 3.5 & 3.6 SD card
#define SDCARD_CS_PIN    BUILTIN_SDCARD
#define SDCARD_MOSI_PIN  11  // not actually used
#define SDCARD_SCK_PIN   13  // not actually used

#include <Bounce2.h>

// 74HC595 Shift Register Configuration and Variables
const byte clockPin = 0;          // Teensy digital pin connected to SH_CP Pin 11 of 74HC595
const byte latchPin = 1;    // Teensy digital pin connected to ST_CP Pin 12 of 74HC595
const byte dataPin = 2  ;           // Teensy digital pin connected to DS of Pin 14 of 74HC595

// Button, Pots and LEDs Configuration
const byte push_buttons[16] = { 33, 34, 35, 36, 37, 38, 39, 15, 16, 17, 18, 19, 20, 21, 22, 23 };

unsigned int previous_push_buttons;
unsigned int current_push_buttons;
unsigned int xor_push_buttons;
unsigned int rising_edge_push_buttons;
unsigned int falling_edge_push_buttons;
byte push_buttons_flag;

byte knob1_potentiometer = A13; //K2

byte knob2_potentiometer = A12; //K1

byte knob3_potentiometer = A0;  // K3

const byte quad_buttons[4] = { 30, 28, 26, 24};
const byte quad_leds[4] = { 29, 27, 25, 12 };
Bounce quad1 = Bounce();
Bounce quad2 = Bounce();
Bounce quad3 = Bounce();
Bounce quad4 = Bounce();

byte const quad_debounce_interval = 3;
byte quad;

const byte function_led = 5;
const byte function_button = 9;

const byte play_led = 6;
const byte play_button = 10;

// Working Variables
const byte number_of_drums = 4;
const byte number_of_patterns = 4;
const byte number_of_steps = 16;

unsigned int pattern[number_of_drums][number_of_patterns];

byte current_drum = 0;
int current_step = 0;
byte current_tick = 0;
const byte max_tick = 24;
byte current_pattern[4];
byte current_pattern_length[4] = { 16, 16, 16, 16 };
byte current_pattern_step[4];

byte current_track1_sample = 1;
byte current_track2_sample = 1;
byte current_track3_sample = 1;
byte current_track4_sample = 1;

byte select_sound = 0;

byte mode;
byte previous_mode;

byte play;
byte play_state;
byte previous_state;

float mixer_gain[4] = { 0.7, 0.7, 0.7, 0.7};

//pot 1
int knob1;
byte knob1_previous;

//pot 2
int knob2;
byte knob2_previous;

//pot 3
int knob3;
byte knob3_previous;

int function = 0;

void setup() {
  setup_push_buttons();
  setup_74HC595();
  setup_quad_buttons();
  setup_quad_leds();
  setup_function_button();
  setup_function_led();
  setup_play_button();
  setup_play_led();
  setup_sounds();
  Serial.begin(57600);

  SPI.setMOSI(SDCARD_MOSI_PIN);
  SPI.setSCK(SDCARD_SCK_PIN);
  if (!(SD.begin(SDCARD_CS_PIN))) {
    // stop here, but print a message repetitively
    while (1) {
      Serial.println("Unable to access the SD card");
      //inserire qui led lampggiante
      delay(500);
    }
  }
}

void loop() {
    update_function_button();
    update_play_button();
    update_quad_buttons();
    update_push_buttons();
    if(play == 1){
      update_timing();
    }
}
