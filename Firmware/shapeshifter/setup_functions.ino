/*
 * SHAPESHIFTER DIY OPEN SOURCE DRUM MACHINE
 * by FASELUNARE.COM
 * based on the DIY DRUM MACHINE by Sebastian Tomczak
 *  
 * https://github.com/faselunare/shapeshifter 
 * Released under the GPLv3 license. 
 * 
 * setup_functions.ino
 * 01.12.2018
 * v.1.1.1
 */

// Set output pins of 74HC595
void setup_74HC595() {
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  for (int i = 0; i < 2; i ++) {
    write_dual_595(255, 255);
    write_dual_595(0, 0);
  }
}

// Configure push buttons as inputs
void setup_push_buttons() {
  for (int i = 0; i < 16; i ++) {
    pinMode(push_buttons[i], INPUT_PULLUP);
  }
}

void setup_quad_buttons() {
  for (int i = 0; i < 4; i ++) {
    pinMode(quad_buttons[i], INPUT_PULLUP);
  }
  quad1.attach(quad_buttons[0]);
  quad1.interval(quad_debounce_interval);
  quad2.attach(quad_buttons[1]);
  quad2.interval(quad_debounce_interval);
  quad3.attach(quad_buttons[2]);
  quad3.interval(quad_debounce_interval);
  quad4.attach(quad_buttons[3]);
  quad4.interval(quad_debounce_interval);
}

void setup_quad_leds() {
  for (int i = 0; i < 4; i ++) {
    pinMode(quad_leds[i], OUTPUT);
  }
}

void setup_function_button() {
  pinMode(function_button, INPUT_PULLUP);
}

void setup_function_led() {
  pinMode(function_led, OUTPUT);
}

void setup_play_button() {
  pinMode(play_button, INPUT_PULLUP);
}

void setup_play_led() {
  pinMode(play_led, OUTPUT);
}

void setup_sounds() {
  AudioMemory(250);
  AudioNoInterrupts();

  filter1.setLowpass(0, 800, 0.54);
  filter1.setLowpass(1, 800, 1.3);
  filter1.setLowpass(2, 800, 0.54);
  filter1.setLowpass(3, 800, 1.3);

  // BitCrusher
  int current_CrushBits = 16; //this defaults to passthrough.
  int current_SampleRate = 22050; // this defaults to passthrough.
  bitcrusher1.bits(current_CrushBits); //set the crusher to defaults. This will passthrough clean at 16,44100
  bitcrusher1.sampleRate(current_SampleRate); //

  for (int i = 0; i < 4; i++) {
    source_mixer.gain(i, mixer_gain[i]);
    source_mixer.gain(i, mixer_gain[i]);
    source_mixer.gain(i, mixer_gain[i]);
    source_mixer.gain(i, mixer_gain[i]);

    trk1_2mix.gain(i, mixer_gain[i]);
    trk1_2mix.gain(i, mixer_gain[i]);
    trk1_2mix.gain(i, mixer_gain[i]);
    trk1_2mix.gain(i, mixer_gain[i]);

    trk3_4mix.gain(i, mixer_gain[i]);
    trk3_4mix.gain(i, mixer_gain[i]);
    trk3_4mix.gain(i, mixer_gain[i]);
    trk3_4mix.gain(i, mixer_gain[i]);

  }
  
  dc1.amplitude(0.7);
  AudioInterrupts();
}
