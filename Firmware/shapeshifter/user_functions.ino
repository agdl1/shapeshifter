/*
 * SHAPESHIFTER DIY OPEN SOURCE DRUM MACHINE
 * by FASELUNARE.COM
 * based on the DIY DRUM MACHINE by Sebastian Tomczak
 *  
 * https://github.com/faselunare/shapeshifter 
 * Released under the GPLv3 license. 
 * 
 * user_functions.ino
 * 01.12.2018
 * v.1.1.1
 */
 
void update_timing() {
  current_tick ++;
  if (current_tick == max_tick) {
    current_tick = 0;
    update_step();
  }
  delay(1 + knob1);
}

void update_step() {
    play_sound();
    
  if (current_step % 4 == 0) {
    // digitalWriteFast(led, HIGH);
    digitalWriteFast(play_led, HIGH);
  }else {
    digitalWriteFast(play_led, LOW);
  }
  if(current_step < 16){
    current_step ++;
  }else{
    current_step = 1;
  }

  knob1 = analogRead(knob1_potentiometer) >> 7; //tempo
  knob2 = analogRead(knob2_potentiometer);
  if (knob2 != knob2_previous) {
     filter1.setLowpass(0, (knob2 * 20) + 10, 0.54);
     filter1.setLowpass(1, (knob2 * 20) + 10, 1.3);
     filter1.setLowpass(2, (knob2 * 20) + 10, 0.54);
     filter1.setLowpass(3, (knob2 * 20) + 10, 1.3);
  }

  knob3 = analogRead(knob3_potentiometer);
  if (knob3 != knob3_previous) {
    bitcrusher1.bits(map(knob3,0, 1023, 0, 16));
  }


  //led scan
//  for(int s=0; s<=16; s++)
//  if (current_step % s == 0) {
    set_single_LED(current_step);
//  }


}

void play_sound() {
  if (bitRead(pattern[0][current_pattern[0]], current_step % current_pattern_length[0])) {
    select_track1_sample();
    digitalWrite(quad_leds[0], HIGH);
  }

  if (bitRead(pattern[1][current_pattern[0]], current_step % current_pattern_length[1])) {
    select_track2_sample();
    digitalWrite(quad_leds[1], HIGH);
  }

  if (bitRead(pattern[2][current_pattern[0]], current_step % current_pattern_length[2])) {
    select_track3_sample();
    digitalWrite(quad_leds[2], HIGH);
  }

  if (bitRead(pattern[3][current_pattern[0]], current_step % current_pattern_length[3])) {
    select_track4_sample();
    digitalWrite(quad_leds[3], HIGH);
  }
}

// Read and update mode button
void update_function_button() {
  mode = 1 - digitalReadFast(function_button);
  if (mode != previous_mode) {
    previous_mode = mode;
    digitalWrite(function_led, mode);
  }
  if (mode == 0) {
    clear_quad_leds();
    digitalWrite(quad_leds[current_drum], HIGH);
    update_LEDs_with_pattern();
  }else {
    clear_quad_leds();
    digitalWrite(quad_leds[current_pattern[current_drum]], HIGH);
    set_LEDs(current_pattern_length[current_drum] + 1);
  }
}

void update_play_button() {
  play_state = 1 - digitalReadFast(play_button);
  if (play_state != previous_state) {
    previous_state = play_state;
    if(play_state){
      if (play == 0) {
        play = 1;
      }else{
        play = 0;
        //current_step = 0;
        digitalWriteFast(play_led, LOW);
      }
    }
  }
}

void update_quad_buttons() {
  quad1.update();
  quad2.update();
  quad3.update();
  quad4.update();

  if (quad1.fell()) {
    switch (mode) {
      case 0:
        if(current_drum != 0) {
          clear_quad_leds();
          digitalWrite(quad_leds[0], HIGH);
          current_drum = 0;
        }
        else {
          set_single_LED(current_track1_sample);
          select_sound = 1;
        }
        break;

      case 1:
        clear_quad_leds();
        digitalWrite(quad_leds[0], HIGH);
        current_pattern[current_drum] = 0;
        break;
    }
  }

  else if (quad2.fell()) {
    switch (mode) {
      case 0:
        if(current_drum != 1) {
          clear_quad_leds();
          digitalWrite(quad_leds[1], HIGH);
          current_drum = 1;
        }
        else {
          set_single_LED(current_track2_sample);
          select_sound = 2;
        }
        break;

      case 1:
        clear_quad_leds();
        digitalWrite(quad_leds[1], HIGH);
        current_pattern[current_drum] = 1;
        break;
    }
  }

  else if (quad3.fell()) {
    switch (mode) {
      case 0:
        if(current_drum != 2) {
          clear_quad_leds();
          digitalWrite(quad_leds[2], HIGH);
          current_drum = 2;
        }
        else {
          set_single_LED(current_track3_sample);
          select_sound = 3;
        }
        break;

      case 1:
        clear_quad_leds();
        digitalWrite(quad_leds[2], HIGH);
        current_pattern[current_drum] = 2;
        break;
    }
  }

  else if (quad4.fell()) {
    switch (mode) {
      case 0:
        if(current_drum != 3) {
          clear_quad_leds();
          digitalWrite(quad_leds[3], HIGH);
          current_drum = 3;
        }
        else {
          set_single_LED(current_track4_sample);
          select_sound = 4;
        }
        break;

      case 1:
        clear_quad_leds();
        digitalWrite(quad_leds[3], HIGH);
        current_pattern[current_drum] = 3;
        break;
    }
  }

  if (quad1.rose() && select_sound == 1 && current_drum == 0) {
    select_sound = 0;
    update_LEDs_with_pattern();
  }

  if (quad2.rose() && select_sound == 2 && current_drum == 1) {
    select_sound = 0;
    update_LEDs_with_pattern();
  }

  if (quad3.rose() && select_sound == 3 && current_drum == 2) {
    select_sound = 0;
    update_LEDs_with_pattern();
  }

  if (quad4.rose() && select_sound == 4 && current_drum == 3) {
    select_sound = 0;
    update_LEDs_with_pattern();
  }
}

// Read push buttons
void update_push_buttons() {
  for (int i = 0; i < 16; i ++) {
    bitWrite(current_push_buttons, i, digitalReadFast(push_buttons[i]));
  }
  if (current_push_buttons != previous_push_buttons) {
    xor_push_buttons = current_push_buttons ^ previous_push_buttons;
    previous_push_buttons = current_push_buttons;
    push_buttons_flag = 1;
  }
  else {
    push_buttons_flag = 0;
  }

  if (push_buttons_flag) {
    find_falling_edge_push_buttons();
    if (mode == 0 && falling_edge_push_buttons > 0 && select_sound == 0) {
      for (int i = 0; i < 16; i ++) {
        if (bitRead(falling_edge_push_buttons, i)  == 1) {
          bitWrite(pattern[current_drum][current_pattern[current_drum]], i, 1 - bitRead(pattern[current_drum][current_pattern[current_drum]], i));
          update_LEDs_with_pattern();
        }
      }
    }

    else if (mode == 1 && falling_edge_push_buttons > 0) {
      byte high_value = 0;
      for (int i = 0; i < 16; i ++) {
        if (bitRead(falling_edge_push_buttons, i)) {
          high_value = i;
        }

      }
      current_pattern_length[current_drum] = high_value;
      set_LEDs(high_value + 1);
    }
    else if (mode == 0 && falling_edge_push_buttons > 0 && select_sound != 0) {
      byte high_value = 0;
      for (int i = 0; i < 16; i ++) {
        if (bitRead(falling_edge_push_buttons, i)) {
          high_value = i + 1;
        }

      }
      switch (select_sound) {
        case 1:
          current_track1_sample = high_value;
          set_single_LED(high_value);
          break;
        case 2:
          current_track2_sample = high_value;
          set_single_LED(high_value);
          break;
        case 3:
          current_track3_sample = high_value;
          set_single_LED(high_value);
          break;
        case 4:
          current_track4_sample = high_value;
          set_single_LED(high_value);
          break;
      }
    }
  }
}

void find_rising_edge_push_buttons() {
  rising_edge_push_buttons = 0;
  for (int i = 0; i < 16; i ++) {
    if (bitRead(xor_push_buttons, i) == 1 && bitRead(current_push_buttons, i) == 1) {
      bitSet(rising_edge_push_buttons, i);
    }
  }
}

void find_falling_edge_push_buttons() {
  falling_edge_push_buttons = 0;
  for (int i = 0; i < 16; i ++) {
    if (bitRead(xor_push_buttons, i) == 1 && bitRead(current_push_buttons, i) == 0) {
      bitSet(falling_edge_push_buttons, i);
    }
  }
}

void set_single_LED(byte single_value) {
  unsigned int work;
  work = 1 << (single_value - 1);
  write_dual_595(byte lowByte(work), byte highByte(work));
}

// Update LEDs with current pattern array area
void update_LEDs_with_pattern() {
  unsigned int work;
  work = pattern[current_drum][current_pattern[current_drum]];
  write_dual_595(byte lowByte(work), byte highByte(work));
}

void set_single_LED_scan(byte single_value) {
  unsigned int work;
  work = 1 << (single_value - 1);
  write_dual_595(byte lowByte(work), byte highByte(work));
}

// Set LEDs to particular linear value (0 - 16)
void set_LEDs(byte set_value) {
  if (set_value == 0) {
    write_dual_595(0, 0);
  }
  else {
    unsigned int work = 0;
    for (int i = 0; i < set_value; i ++) {
      bitSet(work, i);
    }
    write_dual_595(lowByte(work), highByte(work));
  }
}

// Write two bytes to both 595s
void write_dual_595(byte data1, byte data2) {
  digitalWriteFast(latchPin, LOW);
  shiftOut(dataPin, clockPin, MSBFIRST, data2);
  shiftOut(dataPin, clockPin, MSBFIRST, data1);
  digitalWriteFast(latchPin, HIGH);
}

// Reverse the bits of an input byte
byte reverseByte(byte data) {
  data = (data & 0xF0) >> 4 | (data & 0x0F) << 4;
  data = (data & 0xCC) >> 2 | (data & 0x33) << 2;
  data = (data & 0xAA) >> 1 | (data & 0x55) << 1;
  return data;
}

void clear_quad_leds() {
  for (int i = 0; i < 4; i ++) {
    digitalWrite(quad_leds[i], LOW);
  }
}

void select_track1_sample() {
  switch (current_track1_sample) {
    case 1:
      track1.play("tr1_1.wav");
      break;
    case 2:
      track1.play("tr1_2.wav");
      break;
    case 3:
      track1.play("tr1_3.wav");
      break;
    case 4:
      track1.play("tr1_4.wav");
      break;
    case 5:
      track1.play("tr1_5.wav");
      break;
    case 6:
      track1.play("tr1_6.wav");
      break;
    case 7:
      track1.play("tr1_7.wav");
      break;
    case 8:
      track1.play("tr1_8.wav");
      break;
    case 9:
      track1.play("tr1_9.wav");
      break;
    case 10:
      track1.play("tr1_10.wav");
      break;
    case 11:
      track1.play("tr1_11.wav");
      break;
    case 12:
      track1.play("tr1_12.wav");
      break;
    case 13:
      track1.play("tr1_13.wav");
      break;
    case 14:
      track1.play("tr1_14.wav");
      break;
    case 15:
      track1.play("tr1_15.wav");
      break;
    case 16:
      track1.play("tr1_16.wav");
      break;
  }
}


void select_track2_sample() {
  switch (current_track2_sample) {
    case 1:
      track2.play("tr2_1.wav");
      break;
    case 2:
      track2.play("tr2_2.wav");
      break;
    case 3:
      track2.play("tr2_3.wav");
      break;
    case 4:
      track2.play("tr2_4.wav");
      break;
    case 5:
      track2.play("tr2_5.wav");
      break;
    case 6:
      track2.play("tr2_6.wav");
      break;
    case 7:
      track2.play("tr2_7.wav");
      break;
    case 8:
      track2.play("tr2_8.wav");
      break;
    case 9:
      track2.play("tr2_9.wav");
      break;
    case 10:
      track2.play("tr2_10.wav");
      break;
    case 11:
      track2.play("tr2_11.wav");
      break;
    case 12:
      track2.play("tr2_12.wav");
      break;
    case 13:
      track2.play("tr2_13.wav");
      break;
    case 14:
      track2.play("tr2_14.wav");
      break;
    case 15:
      track2.play("tr2_15.wav");
      break;
    case 16:
      track2.play("tr2_16.wav");
      break;
  }
}


void select_track3_sample() {
  switch (current_track3_sample) {
    case 1:
      track3.play("tr3_1.wav");
      break;
    case 2:
      track3.play("tr3_2.wav");
      break;
    case 3:
      track3.play("tr3_3.wav");
      break;
    case 4:
      track3.play("tr3_4.wav");
      break;
    case 5:
      track3.play("tr3_5.wav");
      break;
    case 6:
      track3.play("tr3_6.wav");
      break;
    case 7:
      track3.play("tr3_7.wav");
      break;
    case 8:
      track3.play("tr3_8.wav");
      break;
    case 9:
      track3.play("tr3_9.wav");
      break;
    case 10:
      track3.play("tr3_10.wav");
      break;
    case 11:
      track3.play("tr3_11.wav");
      break;
    case 12:
      track3.play("tr3_12.wav");
      break;
    case 13:
      track3.play("tr3_13.wav");
      break;
    case 14:
      track3.play("tr3_14.wav");
      break;
    case 15:
      track3.play("tr3_15.wav");
      break;
    case 16:
      track3.play("tr3_16.wav");
      break;
  }
}


void select_track4_sample() {
  switch (current_track4_sample) {
    case 1:
      track4.play("tr4_1.wav");
      break;
    case 2:
      track4.play("tr4_2.wav");
      break;
    case 3:
      track4.play("tr4_3.wav");
      break;
    case 4:
      track4.play("tr4_4.wav");
      break;
    case 5:
      track4.play("tr4_5.wav");
      break;
    case 6:
      track4.play("tr4_6.wav");
      break;
    case 7:
      track4.play("tr4_7.wav");
      break;
    case 8:
      track4.play("tr4_8.wav");
      break;
    case 9:
      track4.play("tr4_9.wav");
      break;
    case 10:
      track4.play("tr4_10.wav");
      break;
    case 11:
      track4.play("tr4_11.wav");
      break;
    case 12:
      track4.play("tr4_12.wav");
      break;
    case 13:
      track4.play("tr4_13.wav");
      break;
    case 14:
      track4.play("tr4_14.wav");
      break;
    case 15:
      track4.play("tr4_15.wav");
      break;
    case 16:
      track4.play("tr4_16.wav");
      break;
  }
}
